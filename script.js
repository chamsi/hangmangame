var list_country = [
	"algerie",
	"tunisie",
	"tchad",
	"zimbabwe",
	"nigeria",
	"somalie",
	"soudan",
	"congo",
	"mali",
	"kenya",
	"ghana",

]

let answer = '';
let maxWrong = 6;
let mistakes = 0;
let guessed = [];
let wordStatus = null;
/*  Cette fonction va gérer des mots aleatoires  qui va stocker dans la variable answer 
Math.random(): c'est une fonction predifinie dans package Math qui donne une valeur entre 0 et 1
Math.trunc(): c'est une fonction predifinie dans package Math qui donne une valeur la partie entiere exemple: Math.trunc(1.265)=1 
*/
function randomWord() {
  answer = list_country[Math.trunc(Math.random() * list_country.length)];
}

/*  Cette fonction  va créer des buttons des alphabets de a vers z dans la balise div  de la page index.html.son  identifiant est "keyboard" 
La méthode map () crée des nouveaux buttons avec les résultats de l’appel de la  fonction 
pour chaque élément de la chaine"abcdefghijklmnopqrstuvwzyz" .Elle va gerer la joiture des buttons avec des espaces entre eux. 
*/
function generateButtons() {
  let buttonsHTML = 'abcdefghijklmnopqrstuvwxyz'.split('').map(letter =>
    `
      <button
        class="btn btn-lg btn-primary m-2"
        id='` + letter + `'
        onClick="handleGuess('` + letter + `')"
      >
        ` + letter + `
      </button>
    `).join('');
  document.getElementById('keyboard').innerHTML = buttonsHTML;
}
/********* */
function handleGuess(chosenLetter) {
  guessed.indexOf(chosenLetter) === -1 ? guessed.push(chosenLetter) : null;
  document.getElementById(chosenLetter).setAttribute('disabled', true);

  if (answer.indexOf(chosenLetter) >= 0) {
    guessedWord();
    checkIfGameWon();
  } else if (answer.indexOf(chosenLetter) === -1) {
    mistakes++;
    updateMistakes();
    checkIfGameLost();
    updateHangmanPicture();
  }
}
/* l'intervalle de mistakes[0,6] et les images ses noms sont 0.jpg,1.jpg... on a créer les noms des images 
meme valeurs de miskes pour assurer la changemant des images automatiquemet plus au moins */
function updateHangmanPicture() {
  document.getElementById('hangmanPic').src = './images/' + mistakes + '.jpg';
}
/* tester si le mot valide ou non en affichant unn message "you won"*/
function checkIfGameWon() {
  if (wordStatus === answer) {
    document.getElementById('keyboard').innerHTML = ' Won!!!';
  }
}
/* cette fonction va tester si le mot est incorrecte on affichant un message d'echec et le mot cherché */
function checkIfGameLost() {
  if (mistakes === maxWrong) {
    document.getElementById('wordSpotlight').innerHTML = 'The answer was: ' + answer;
    document.getElementById('keyboard').innerHTML = 'Wrong!!!';
  }
}
/* cette fonction va remplacer les alphabets de mot par des "-"  en utilisant la fonction map() */ 
function guessedWord() {
  wordStatus = answer.split('').map(letter => (guessed.indexOf(letter) >= 0 ? letter : " _ ")).join('');

  document.getElementById('wordSpotlight').innerHTML = wordStatus;
}
/* changer le contenu de l'element mistakes*/
function updateMistakes() {
  document.getElementById('mistakes').innerHTML = mistakes;
}

/* function reset() assure l'initialisation des champs  et l'image par defaut 0.jpg */ 
function reset() {
  mistakes = 0;
  guessed = [];
  document.getElementById('hangmanPic').src = './images/0.jpg';

  randomWord();
  guessedWord();
  updateMistakes();
  generateButtons();
}

document.getElementById('maxWrong').innerHTML = maxWrong;

randomWord();
generateButtons();
guessedWord();
